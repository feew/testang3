import { Injectable, Inject } from '@angular/core';
// import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class UserService {

  constructor(private authHttp: AuthHttp, @Inject('API_URL') private url: string) { }

  // async Test() {
  //   let url = 'https://randomuser.me/api/?results=200';
  //   let rs: any = await this.http.get(url).toPromise();
  //   return rs.json();
  // }

  async getUserTypesList() {
    let url = this.url + '/api/user-types';
    let rs: any = await this.authHttp.get(url).toPromise();
    return rs.json();
  }

  async getUsers() {
    let url = this.url + '/users/detail';
    let rs: any = await this.authHttp.get(url).toPromise();
    return rs.json();
  }

  async getLatLng(userId: any) {
    let url = this.url + '/api/users/maps/' + userId;
    let rs: any = await this.authHttp.get(url).toPromise();
    return rs.json();
  }

  async updateLatLng(userId: any, lat: any, lng: any) {
    let url = this.url + '/api/users/maps/' + userId;
    let rs: any = await this.authHttp.put(url, {
      lat: lat,
      lng: lng
    }).toPromise();
    return rs.json();
  }

  async remove(userId: any) {
    let url = this.url + '/users/detail/' + userId;
    let rs: any = await this.authHttp.delete(url).toPromise();
    return rs.json();
  }

  async getUserDetail(userId: any) {
    let url = this.url + '/users/detail/' + userId;
    let rs: any = await this.authHttp.get(url).toPromise();
    return rs.json();
  }

  async getUserTypes() {
    let url = this.url + '/users/detail/';
    let rs: any = await this.authHttp.get(url).toPromise();
    return rs.json();
  }

  async save(username: any, password: any, firstName: any, lastName: any) {
    let url = this.url + '/users/detail';
    let rs: any = await this.authHttp.post(url, {
      username: username,
      password: password,
      fname: firstName,
      lname: lastName,
    }).toPromise();
    return rs.json();
  }

  async update(userId: any, password: any, firstName: any, lastName: any) {
    let url = this.url + '/users/detail/' + userId;
    let rs: any = await this.authHttp.put(url, {
      password: password,
      fname: firstName,
      lname: lastName
    }).toPromise();
    return rs.json();
  }
}
