import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from '../user.service';
//import { SocketService } from '../../socket.service';

//import { Socket } from 'ng-socket-io';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

  users: any = [];
  loding = false;
  selected = [];

  constructor(
    private userService: UserService, 
    //private socketService: SocketService,
    //private socket: Socket
  ) { }

  ngOnInit() {

    //this.socketService.sendWelcome('Hello');

    this.getUsers();
  }

  sendAddUser() {
    //this.socket.emit('adduser', 'Hello world!!');

  }

  async remove(user: any,id:any) {
    if (confirm('Are you sure? ['+ user.username +']')) {
      let rs = await this.userService.remove(id);
      if (rs.ok) {
         this.getUsers();
        //this.users.splice(id, 1);
      }
    }
  }

  async getUsers() {
    this.loding = true;
    let rs = await this.userService.getUsers();
    // console.log(rs);
    this.users = rs.rows;
    this.loding = false;
  }

}
