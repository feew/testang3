import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class NewUserComponent implements OnInit {

  username: any;
  password: any;
  firstName: any;
  lastName: any;
  id:any;

  userId: any;

  types: any = [];

  errorMessage: string;
  isError: boolean = false;
  saving: boolean = false;

  constructor(
    private userService: UserService, 
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.userId = this.route.snapshot.params.userId;
   }

  async ngOnInit() {

    if (this.userId) {
      let rs = await this.userService.getUserDetail(this.userId);
      this.id=rs.rows.id;
      this.username = rs.rows.username;
      this.firstName = rs.rows.fname;
      this.lastName = rs.rows.lname;
      this.password = rs.rows.password;
    }
  }


  async save() {
    this.saving = true;
    try {
    let rs = null;

    if (this.userId) {
      rs = await this.userService.update(
        this.userId, 
        this.password, 
        this.firstName, 
        this.lastName);
    } else {
      rs = await this.userService.save(
        this.username, 
        this.password, 
        this.firstName, 
        this.lastName);
    }

    if (rs.ok) {
      this.router.navigate(['/admin']);
    } else {
      this.saving = false;
      this.isError = true;
      this.errorMessage = rs.error;
    }
    } catch (error) {
      console.log(error);
      this.isError = true;
      if (error.status === 0) {
        this.errorMessage = 'ไม่สามารถเชื่อมต่อกับ Server ได้';
      } else {
        this.errorMessage = 'Server error: codt=' + error.status;
      }
      this.saving = false;
    }
  }

}
